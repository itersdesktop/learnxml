<?xml version="1.0" encoding="ISO-8859-1"?>
<!-- Edited by XMLSpy® -->
<xsl:stylesheet version="1.0"
xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
  <html>
  <body>
  <h2>My CD Collection</h2>  
  <xsl:apply-templates/>  
  </body>
  </html>
</xsl:template>

<xsl:template match="cd">
  <p>
    <xsl:apply-templates select="artist"/>, 
    <xsl:apply-templates select="title"/>  
  </p>
</xsl:template>

<xsl:template match="title">
  <span style="color:#ff0000; font-style:italic">
  <xsl:value-of select="."/></span>
</xsl:template>

<xsl:template match="artist">
  <span style="color:#00ff00">
  <xsl:value-of select="."/></span>
</xsl:template>

</xsl:stylesheet>
