<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <HTML>
        <HEAD>
            <TITLE>
                Classe 3A
            </TITLE>
        </HEAD>
        <BODY>
            <H1>
                Classe de 3ème A
            </H1>

            <TABLE Border="2">
                <TR>
                    <TD>NOM</TD>
                    <TD>Prénom</TD>
                    <TD>Age</TD>
                    <TD>Redoublant</TD>
                </TR>
                <xsl:apply-templates />
            </TABLE>
        </BODY>
    </HTML>

</xsl:template>

<xsl:template match="Eleve">

    <TR>

        <TD><xsl:value-of select="Nom" /></TD>

        <TD><xsl:value-of select="Prenom" /></TD>

        <TD><xsl:apply-templates select="Age" /></TD>

        <TD><xsl:value-of select="Redoub" /></TD>

    </TR>

</xsl:template>

<xsl:template match="Age">

    <xsl:value-of select="." />

    <xsl:text>: </xsl:text>

    <xsl:value-of select="@Unite" />

</xsl:template>

</xsl:stylesheet>
