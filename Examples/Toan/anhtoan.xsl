<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<xsl:template match="/">
    <HTML>
        <HEAD>
            <TITLE>
                Classe 3A
            </TITLE>
        </HEAD>
        <BODY>
            <H1>
                Classe de 3ème A
            </H1>

            <TABLE Border="1">
                <TR>
                    <TD style="background-color:blue">NOM</TD>
                    <TD style="background-color:blue">Prénom</TD>
                    <TD style="background-color:blue">Age</TD>
                    <TD style="background-color:blue">Redoublant</TD>
                </TR>
                <xsl:apply-templates />
            </TABLE>
        </BODY>
    </HTML>

</xsl:template>

<xsl:template match="Eleve">

    <TR>

        <TD><xsl:value-of select="Nom" /></TD>

        <TD><xsl:value-of select="Prenom" /></TD>

        <TD><xsl:apply-templates select="Age" /></TD>

        <TD><xsl:value-of select="Redoub" /></TD>

    </TR>

</xsl:template>

<xsl:template match="Age">
	<xsl:value-of select="@Unite" />
	<xsl:text> </xsl:text>
    <xsl:value-of select="." />
    <xsl:text> </xsl:text>
	<xsl:value-of select="@sex" />
</xsl:template>

</xsl:stylesheet>
